package com.ivanferdino.teknikal.model;

import java.util.List;

public class Dogs {
    private String breed;
    private List sub_breed;

    public Dogs(){

    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public List getSub_breed() {
        return sub_breed;
    }

    public void setSub_breed(List sub_breed) {
        this.sub_breed = sub_breed;
    }
}

package com.ivanferdino.teknikal.dao;

import com.ivanferdino.teknikal.model.Salary;
import com.ivanferdino.teknikal.repository.SalaryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SalaryDAO {
    @Autowired
    SalaryRepository salaryRepository;

    //insert new salary record
    public Salary save(Salary salary){
        return salaryRepository.save(salary);
    }

    //select salary by emp id
    public Salary getSalaryByEmpId(Integer id){
        return salaryRepository.findById(id).orElse(null);
    }

    //delete by obj
    public void deleteSalary(Salary salary){
        salaryRepository.delete(salary);
    }
}

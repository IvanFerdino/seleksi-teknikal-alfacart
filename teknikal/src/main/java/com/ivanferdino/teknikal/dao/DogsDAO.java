package com.ivanferdino.teknikal.dao;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


@Service
public class DogsDAO {
    public Object getBreed(){
        //url string for data source
        final String uri = "https://dog.ceo/api/breeds/list";

        RestTemplate restTemplate = new RestTemplate();
        Object result = restTemplate.getForObject(uri, Object.class);
        return result;
    }
    public Object getSubBreed(String subBreed){
        //url string for data source
        final String uri = "https://dog.ceo/api/breed/"+subBreed+"/list";

        RestTemplate restTemplate = new RestTemplate();
        Object result = restTemplate.getForObject(uri, Object.class);
        return result;
    }
    public Object getImageByBreed(String breed){
        //url string for data source
        final String uri = "https://dog.ceo/api/breed/"+breed+"/images";

        RestTemplate restTemplate = new RestTemplate();
        Object result = restTemplate.getForObject(uri, Object.class);
        return result;
    }
}

package com.ivanferdino.teknikal.dao;

import com.ivanferdino.teknikal.model.Employee;
import com.ivanferdino.teknikal.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeDAO {
    //data access object, CRUD
    @Autowired
    EmployeeRepository employeeRepository;

    //untuk cek emp_id
    public boolean checkId(Integer emp_id){
        Employee tmp = employeeRepository.findById(emp_id).orElse(null);
        if(tmp==null){
            return false;
        }
        return true;
    }

    //untuk cek role id
    public boolean checkRoleId(Integer role_id){
        Object tmp = employeeRepository.findRoleId(role_id);
        if(tmp==null){
            return false;
        }
        return true;
    }

    //get completeEmployeeData, join tabel role dan salary
    public List getDetail(){
        return employeeRepository.getCompleteEmployeeData();
    }

    //get employee by id, join tabel role dan salary
    public Object getEmpById(Integer id){
        return employeeRepository.getEmployeeById(id);
    }

    //insert, employee object
    public Employee save(Employee emp){
        return employeeRepository.save(emp);
    }

    //select all, dari tabel employee saja
    public List<Employee> getAll(){
        return employeeRepository.findAll();
    }

    //select one, dari tabel employee saja
    public Employee getEmployeeById(Integer id){
        return employeeRepository.findById(id).orElse(null);
    }

    //delete by obj, employee object
    public void deleteEmp(Employee emp){
        employeeRepository.delete(emp);
    }
}

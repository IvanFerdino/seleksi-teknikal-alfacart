package com.ivanferdino.teknikal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class TeknikalApplication {

	public static void main(String[] args) {
		SpringApplication.run(TeknikalApplication.class, args);
	}

}

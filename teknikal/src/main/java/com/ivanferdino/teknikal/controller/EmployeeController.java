package com.ivanferdino.teknikal.controller;

import com.ivanferdino.teknikal.dao.EmployeeDAO;
import com.ivanferdino.teknikal.dao.SalaryDAO;
import com.ivanferdino.teknikal.exception.ApiRequestException;
import com.ivanferdino.teknikal.model.Employee;
import com.ivanferdino.teknikal.model.Salary;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@RestController
//@RequestMapping("/company")
public class EmployeeController {    //disini yg menerima requests
    private final String msg1 = "Maaf id tidak ditemukan";
    private final String msg2 = "Maaf role_id tidak valid";
    private final String msg3 = "Maaf salary hanya boleh dalam bentuk angka";

    //create DAO object
    @Autowired
    EmployeeDAO employeeDAO;
    @Autowired
    SalaryDAO salaryDAO;

    //make a method
    public boolean checkEmpId(Integer emp_id){//untuk check apakah employee id tersedia/ditemukan
        if(employeeDAO.checkId(emp_id)){
            return true;
        }
        return false;
    }

    public boolean checkRoleId(Integer role_id){// untuk check apakah role id tersedia/ditemukan
        if(employeeDAO.checkRoleId(role_id)){
            return true;
        }
        return false;
    }

    @PostMapping("/employee/{id}")//untuk mapping request method POST, insert
    public ResponseEntity updateById(@PathVariable(value="id") Integer emp_id, @Valid @RequestBody Map<String, Object> updateObj){//request body di masukkan kedalam Map, dengan key bentuk String, dan value dalam bentuk Object
        if(!checkEmpId(emp_id)){//cek employee id
            throw new ApiRequestException(msg1);
        }else{
            if(updateObj.equals(null)) {//cek updateObject dari request body memiliki konten
                return ResponseEntity.notFound().build();
            }else{
                //parse ke timestamp, dari string
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
                LocalDateTime localDateTime = LocalDateTime.from(formatter.parse(updateObj.get("dob").toString()));
                Timestamp dob =Timestamp.valueOf(localDateTime);

                //cek apakah salary dalam bentuk angka
                String regex = "[0-9]+";
                if(!updateObj.get("salary").toString().matches(regex)){
                    throw new ApiRequestException(msg3);
                }

                //get selected id employee detail
                Employee emp = employeeDAO.getEmployeeById(emp_id);
                Salary salary = salaryDAO.getSalaryByEmpId(emp_id);

                //update the data
                emp.setFull_name(updateObj.get("full_name").toString());
                emp.setDob(dob);
                salary.setSalary(Integer.valueOf(updateObj.get("salary").toString()));

                //save the new data/updated
                employeeDAO.save(emp);
                salaryDAO.save(salary);
                return ResponseEntity.ok().build();
            }
        }
    }

    @GetMapping("/employee")
    public  List<JSONObject> getAll(){
        //untuk mendapatkan hasil query
        List<Object[]> result = null;
        result=employeeDAO.getDetail();

        //untuk output
        List<JSONObject> output = new ArrayList<JSONObject>();//ini untuk menampung hasil sebelum dikeluarkan/di return

        //di loop hasil dari query nya
        Iterator iter = result.iterator();
        while(iter.hasNext()){
            //masukin ke tmp
            Object[] tmp = (Object[]) iter.next();

            //buat json tmp, untuk menyusun JSON
            JSONObject tmpJson = new JSONObject();

            //ambil isi tmp masukkan ke variable
            Integer id = (Integer) tmp[0];
            String full_name = (String) tmp[1];
            String address = (String) tmp[2];
            Timestamp temp = ((Timestamp) tmp[3]);
            String dob = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(temp);
            Integer role_id = (Integer) tmp[4];
            String role_name = (String) tmp[5];
            Integer salary = (Integer) tmp[6];

            //susun ulang supaya ada key nya, karna hasil query gada key, hanya value nya saja
            tmpJson.put("id",id);
            tmpJson.put("full_name",full_name);
            tmpJson.put("address",address);
            tmpJson.put("dob",dob);
            tmpJson.put("role_id",role_id);
            tmpJson.put("role_name",role_name);
            tmpJson.put("salary",salary);

            //masukkan JSON baru ke list output
            output.add(tmpJson);
        }
        return output;
    }

    @GetMapping("/employee/{id}")
    public ResponseEntity<JSONObject> getById(@PathVariable(value="id") Integer emp_id){
        if(!checkEmpId(emp_id)){//cek employee id
            throw new ApiRequestException(msg1);
        }else{
            Object result = employeeDAO.getEmpById(emp_id);//get employee by id
            if(result.equals(null)) {//cek apakah result ditemukan
                return ResponseEntity.notFound().build();
            }else{
                Object[] tmp = (Object[]) result;//cast result menjadi array of object
                //buat json tmp
                JSONObject tmpJson = new JSONObject();

                //ambil isi tmp masukiin ke var
                Integer id = (Integer) tmp[0];
                String full_name = (String) tmp[1];
                String address = (String) tmp[2];
                Timestamp temp = ((Timestamp) tmp[3]);
                String dob = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(temp);
                Integer role_id = (Integer) tmp[4];
                String role_name = (String) tmp[5];
                Integer salary = (Integer) tmp[6];

                //susun ulang supaya ada key nya, karna hasil query gada key
                tmpJson.put("id",id);
                tmpJson.put("full_name",full_name);
                tmpJson.put("address",address);
                tmpJson.put("dob",dob);
                tmpJson.put("role_id",role_id);
                tmpJson.put("role_name",role_name);
                tmpJson.put("salary",salary);

                return ResponseEntity.ok().body(tmpJson);
            }
        }
    }

    @DeleteMapping("/employee/{id}")
    public ResponseEntity<Employee> deleteEmployeeById(@PathVariable(value="id") Integer emp_id){
        if(!checkEmpId(emp_id)){//cek employee id
            throw new ApiRequestException(msg1);
        }else {
            Employee emp = employeeDAO.getEmployeeById(emp_id);//get employee data
            if(emp.equals(null)) {//cek apakah ditemukan
                return ResponseEntity.notFound().build();
            }else{
                Salary salary = salaryDAO.getSalaryByEmpId(emp.getId());//get employee salary dari employee id
                if(salary.equals(null)) {//cek apakah ditemukan
                    return ResponseEntity.notFound().build();
                }else{
                    //lakukan delete
                    salaryDAO.deleteSalary(salary);
                    employeeDAO.deleteEmp(emp);
                    return ResponseEntity.ok().build();
                }
            }
        }
    }

    @PutMapping("/employee")
     public ResponseEntity insert(@Valid @RequestBody Map<String, Object> insertObj){
        if(insertObj.equals(null)) {//cek insertObj apakah memiliki konten
            return ResponseEntity.notFound().build();
        }else{
            //parse ke timestamp, dari string
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
            LocalDateTime localDateTime = LocalDateTime.from(formatter.parse(insertObj.get("dob").toString()));
            Timestamp dob =Timestamp.valueOf(localDateTime);

            //cek apakah salary dalam bentuk angka
            String regex = "[0-9]+";
            if(!insertObj.get("salary").toString().matches(regex)){
                throw new ApiRequestException(msg3);
            }

            //buat object employee baru
            Employee emp = new Employee();
            emp.setFull_name(insertObj.get("full_name").toString());
            emp.setAddress(insertObj.get("address").toString());
            emp.setDob(dob);

            //cek role apakah tersedia/ditemukan
            if(!checkRoleId(Integer.valueOf(insertObj.get("role_id").toString()))){
                throw new ApiRequestException(msg2);
            }
            emp.setRole_id(Integer.valueOf(insertObj.get("role_id").toString()));
            emp = employeeDAO.save(emp);// save new employee record

            //insert salary record
            Salary salary = new Salary();
            salary.setEmployee_id(emp.getId());//get last inserted emp id
            salary.setSalary(Integer.valueOf(insertObj.get("salary").toString()));
            salaryDAO.save(salary);//insert salary record
            return ResponseEntity.ok().build();
        }
    }

}

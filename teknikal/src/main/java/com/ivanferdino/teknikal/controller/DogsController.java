package com.ivanferdino.teknikal.controller;

import com.ivanferdino.teknikal.dao.DogsDAO;
import com.ivanferdino.teknikal.model.Dogs;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;


import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
public class DogsController {
    @Autowired
    DogsDAO dogsDAO;
    @GetMapping("/dogs")
    public ResponseEntity getBreed(){
        Map<String, Object> result = (Map<String, Object>) dogsDAO.getBreed(); //get breed list
        ArrayList arr = (ArrayList) result.get("message");//get breed list name only
        List<Dogs> dogDetails = new ArrayList<Dogs>();//unutk menampung output/hasil untuk di return

        for (Object tmp: arr) {//loop breed list name
            String breed = tmp.toString();//tampung namanya

            //get subbreed nya berdasarkan breed name
            Map<String, Object> res = (Map<String, Object>) dogsDAO.getSubBreed(breed);//get sub breed list
            ArrayList ar = (ArrayList) res.get("message");//get sub breed name only
            List<Dogs> subbreed_list = new ArrayList<Dogs>();//untuk menampung hasil sub breed
            if(ar.size()>0){//cek apakah breed memiliki subbreed
                for(Object temp:ar){//loop setiap subbreed name
                    String subbreed = temp.toString();//get name
                    Dogs dog = new Dogs();//buat dogs object baru
                    dog.setBreed(subbreed);
                    dog.setSub_breed(null);
                    subbreed_list.add(dog);//tambahkan ke array subbreed dari breed ini
                }
            }else{//jika tidak punya subbreed, subbreed array langsung null
                subbreed_list=null;
            }
            //setelah selesai loop, buat object dogs baru, dan masukkan subbreed list tadi
            Dogs dog = new Dogs();
            dog.setBreed(breed);
            dog.setSub_breed(subbreed_list);

            //add ke list hasil akhir
            dogDetails.add(dog);
        }
        return ResponseEntity.ok().body(dogDetails);
    }

    @GetMapping("/dogs/{breed_name}")
    public ResponseEntity<JSONObject> getBreed(@PathVariable(value="breed_name") String breed_name){
        //get subbreed nya
        Map<String, Object> res = (Map<String, Object>) dogsDAO.getSubBreed(breed_name);//get subbreed data
        Map<String, Object> images = (Map<String, Object>) dogsDAO.getImageByBreed(breed_name);//get breed images

        ArrayList imgArr = (ArrayList) images.get("message");//get images link only
        ArrayList ar = (ArrayList) res.get("message");//get subbreed name only
        List<Dogs> subbreed_list = new ArrayList<Dogs>();//untuk menampung hasil akhir
        if(ar.size()>0){//cek apakah memiliki subbreed
            for(Object temp:ar){//loop untuk setiap subbreed
                String subbreed = temp.toString();
                Dogs dog = new Dogs();
                dog.setBreed(subbreed);
                dog.setSub_breed(null);
                subbreed_list.add(dog);//tambahkan ke subbreed list
            }
        }else{//jika tidak punya subbreed
            subbreed_list=null;
        }

        //buat json object untuk di return
        JSONObject jsonObjOut = new JSONObject();
        jsonObjOut.put("images",imgArr);
        jsonObjOut.put("breed",breed_name);
        jsonObjOut.put("sub_breed",subbreed_list);

        return ResponseEntity.ok().body(jsonObjOut);
    }
}

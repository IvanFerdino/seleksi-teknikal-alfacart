package com.ivanferdino.teknikal.repository;

import com.ivanferdino.teknikal.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee,Integer> {
    @Query(value = "SELECT e.id AS 'id', e.full_name AS 'full_name', \n" +
            "e.address AS 'address', e.dob AS 'dob', e.role_id AS 'role_id', r.role_name AS 'role_name',\n" +
            "s.salary AS 'salary' FROM employee e JOIN role r ON e.role_id=r.role_id JOIN salary s ON e.id=s.employee_id",
            nativeQuery = true)
    List getCompleteEmployeeData();

    @Query(value = "SELECT e.id AS 'id', e.full_name AS 'full_name', \n" +
            "e.address AS 'address', e.dob AS 'dob', e.role_id AS 'role_id', r.role_name AS 'role_name',\n" +
            "s.salary AS 'salary' FROM employee e JOIN role r ON e.role_id=r.role_id JOIN salary s ON e.id=s.employee_id WHERE e.id = :emp_id",
            nativeQuery = true)
    Object getEmployeeById(@Param("emp_id") Integer emp_id);

    @Query(value = "SELECT * FROM role WHERE role_id = :role_id",
            nativeQuery = true)
    Object findRoleId(@Param("role_id") Integer role_id);

}

